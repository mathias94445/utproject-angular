import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EditUserComponent } from './components/edit-user/edit-user.component';
import { ListUserComponent } from './components/list-user/list-user.component';
import { LoginComponent } from './components/login/login.component';
import { PredictComponent } from './components/predict/predict.component';
import { ProjectComponent } from './components/project/project.component';
import { RegisterComponent } from './components/register/register.component';
import { AuthGuard } from './guard/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: 'login',pathMatch:'full' },
  { 
    path: 'login', 
    component: LoginComponent },
  {
    path: 'login/register',
    component: RegisterComponent
  },
  {
    path: 'project',
    component: ProjectComponent,
    canActivate:[AuthGuard],
  },
  {
    path: 'users',
    component: ListUserComponent,
    canActivate:[AuthGuard]
  },
  {
    path: 'edit-user',
    component: EditUserComponent,
    canActivate:[AuthGuard]
  },
  {
    path: 'predict',
    component: PredictComponent,
    canActivate:[AuthGuard]
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
