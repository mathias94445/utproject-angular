import { Component } from '@angular/core';
import { SharedServiceService } from './service/shared-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'UTProject-Angular';
  nombre =   'Usuario'
  email = 'Correo'

  signOut(){
    console.log("signOut")
    localStorage.clear();
  }
  constructor(
    private _sharedService: SharedServiceService
) {
      _sharedService.changeEmitted$.subscribe(
    text => {
        this.nombre = text.nombre|| '' + text.primerapellido|| '' +  text.segundoapellido|| '' ;
        this.email = text.email;
        console.log(text);
    });
  }


}
