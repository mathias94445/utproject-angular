import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { NewProject } from '../models/new-project';
import { Project } from '../models/project';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  private serviceUrl = environment.serviceUrl;

  constructor(private http: HttpClient, private route:Router) { }
  
  getProjects() {
    return this.http.get<Project[]>(`${this.serviceUrl}obtenerproyectos/`);
  }
  getProjectId(id:string) {
    return this.http.get<NewProject>(`${this.serviceUrl}obtenerproyectoId/${id}`);
  }
  putProjectId(id:string,project:NewProject) {
    return this.http.put(`${this.serviceUrl}actualizarproyectoId/${id}`,project);
  }
  addProject(project:NewProject) {
    return this.http.post(`${this.serviceUrl}registrarNuevoProyecto/`,project);
  }
}
