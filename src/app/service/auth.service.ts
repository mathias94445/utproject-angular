import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { User } from '../models/user.model';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private serviceUrl = environment.serviceUrl;
  constructor(private http: HttpClient, private route:Router) { }

  login(email:any, password:any) {
    const body = {
      email: email,
      password: password
    };
    //console.log(`${this.serviceUrl}auth/iniciasesion`);
    //console.log(body);
    return this.http.post(`${this.serviceUrl}iniciasesion`, body);
  }

  register(nombre:string,email:string, password:string) {
    const body = {
      nombre: nombre,
      email: email,
      password: password
    };
    return this.http.post(`${this.serviceUrl}registrarse`, body);
  }

  logout(){
    localStorage.removeItem('id');
    this.route.navigate(['/']);
    
  }
  
  isLogged(){
    return !!localStorage.getItem('sesion')||!!localStorage.getItem('id');
  }
}
