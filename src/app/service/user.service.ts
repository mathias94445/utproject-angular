import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private serviceUrl = environment.serviceUrl;
  user = {
    nombre: "",
    primerapellido: "",
    segundoapellido: "",
    email: "",
    rol: ""
  };
  constructor(private http: HttpClient, private route:Router) { }
  
  getUser(id:string) {
    return this.http.get<User>(`${this.serviceUrl}obtenerPerfil/${id}`);
  }

  editUser(id:string,user:any){
    
    return this.http.put(`${this.serviceUrl}actualizarPerfil/${id}`,user);
  }

}
