import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { Project } from '../models/project';
import { User } from '../models/user.model';


@Injectable({
  providedIn: 'root'
})
export class ResourcesService {

  private serviceUrl = environment.serviceUrl;

  constructor(private http: HttpClient, private route:Router) { }

  getResources() {
    
    return this.http.get<User[]>(`${this.serviceUrl}lstUsuariosAasignar/`);
  }
  getProjects() {
    
    return this.http.get<Project[]>(`${this.serviceUrl}obtenerproyectos/`);
  }
  addResourceToProject(id_Project:string,id_Resource:string) {
    const body = {
      IdProyecto : id_Project,
      IdUsuario : id_Resource
    };
    return this.http.post<Project[]>(`${this.serviceUrl}AsignarRecursoProyectoPorIdUsuario/`,body);
  }
  getProjectsResource(id:string){

    return this.http.get<Project[]>(`${this.serviceUrl}obtenerDetalleProyectosPorIdUsuario/${id}`);
  }

}
