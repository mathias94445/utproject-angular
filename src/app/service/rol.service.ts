import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { Rol } from '../models/rol';

@Injectable({
  providedIn: 'root'
})
export class RolService {
  private serviceUrl = environment.serviceUrl;

  constructor(private http: HttpClient, private route:Router) { }
  
  getRols() {
    
    return this.http.get<Rol[]>(`${this.serviceUrl}obtenerRoles/`);
  }
  getRol(id:string) {
    
    return this.http.get<Rol>(`${this.serviceUrl}obtenerRolId/${id}`);
  }

}
