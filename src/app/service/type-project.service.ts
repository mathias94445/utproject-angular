import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { Project } from '../models/project';
import { TypeProject } from '../models/type-project';

@Injectable({
  providedIn: 'root'
})
export class TypeProjectService {
  private serviceUrl = environment.serviceUrl;
  constructor(private http: HttpClient, private route:Router) { }

  getTypeProjects() {
    
    return this.http.get<TypeProject[]>(`${this.serviceUrl}obtenerTipoProyectos/`);
  }
  getTypeProject(id:string) {
    
    return this.http.get<TypeProject>(`${this.serviceUrl}obtenerTipoProyectoId/${id}`);
  }
}
