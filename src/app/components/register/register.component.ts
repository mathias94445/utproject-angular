import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/service/auth.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  
  public user = {
    nombre: "",
    apellido: "",
    email: "",
    password: ""
  };
  signInError: boolean | undefined;
  ErrorMxg: string | undefined;
  ValidatePass:String | undefined;
  ValidatePass2:String | undefined;
  ValidateNombre:String | undefined;
  ValidateApellido:String | undefined;
  ValidateEmail:String | undefined;
  ValidateTotal:String = "disabled";
  pass2:string="";

  constructor(public authService:AuthService,
    private router: Router) { }

  ngOnInit(): void {
  }

  register(){
    this.authService.register(this.user.nombre + this.user.apellido,this.user.email,this.user.password)
    .subscribe((response:any)=>{
      console.log(response);
      this.signInError=false;
      localStorage.setItem('id',response);
      this.router.navigate(['project']);
    },error=>{
      console.log(error)
      this.signInError=true;
      this.ErrorMxg=error;
    })
  }
    
    
    isEmail()
    {
      var serchfind: boolean;
      let regexp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
      serchfind = regexp.test(this.user.email);
      if (!serchfind) {
        this.ValidateEmail = "invalid";

      } else {
        this.ValidateEmail = "valid";
      }
    }

    isNullNombre(input:string){
      if(input.length>0){
        this.ValidateNombre = "valid";
      }else{
        this.ValidateNombre = "invalid";
      }
      
    }
    isNullApellido(input:string){
      if(input.length>0){
        this.ValidateApellido = "valid";
      }else{
        this.ValidateApellido = "invalid";
      }
      
    }
    isNullPass1(input:string){
      if (input.length<5) {
        this.ValidatePass="invalid";
      }else{
        this.ValidatePass="valid";
      }
    }
    isNullPass2(input:string){
      if (input.length>5 && this.user.password == input) {
       this.ValidatePass2="valid";
        this.ValidatePass="valid"; 
        this.ValidateTotal=""
      }else{
        this.ValidatePass2="invalid";
        this.ValidatePass="invalid";
        this.ValidateTotal="disabled"
      }
      
    }

}
