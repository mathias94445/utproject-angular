import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/service/auth.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public rememberLoginControl = new FormControl();
  
  user ={
    email:'',
    password:''
  }
  @Input()
  signInError: boolean | undefined;
  signInState: boolean | undefined;

  constructor(public authService:AuthService,
    private router: Router
    ) { }

  ngOnInit(): void {
  
    if(localStorage.getItem('sesion')){
      //console.log('sesion activa')
      this.router.navigate(['project']);
    }else{
      localStorage.clear();
    }
  }

  login(){
    this.authService.login(this.user.email,this.user.password)
    .subscribe((response:any)=>{
      //console.log(response);
      this.signInError=false;
      localStorage.setItem('id',response);
      //console.log(this.rememberLoginControl.value)
      if(this.rememberLoginControl.value){localStorage.setItem('sesion','activa');}
      this.router.navigate(['project']);
    },error=>{
      //console.log(error)
      this.signInError=true;
    })}
}
