import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/service/user.service';
import * as M from 'materialize-css';
import { RolService } from 'src/app/service/rol.service';
import { Rol } from 'src/app/models/rol';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})

export class EditUserComponent implements OnInit {

  constructor(private router: Router, 
    public userService:UserService, 
    public rolServide:RolService) { }
  
 
  public id:any = localStorage.getItem('id');

  signInError: boolean=false;
  ErrorMxg: string="";
  ValidatePass:String="";
  ValidatePass2:String="";
  ValidateNombre:String="";
  ValidateApellido:String="";
  ValidateEmail:String="";
  ValidateTotal:String = "disabled";
  pass2:string = "";
  arrRol:Rol[] = [];
  Rol:Rol = new Rol;
  
  public userGet:User=new User();
  

  ngOnInit(): void {
    
    this.getUser();
    this.getRol();
    setTimeout(function(){ $('.input-field label').addClass('active'); }, 1);
    setTimeout(function(){ $('#slec').removeClass('active'); }, 1);
    document.addEventListener('DOMContentLoaded', function() {
      var elems = document.querySelectorAll('select');
      var instances = M.FormSelect.init(elems);
    });
    setTimeout(function(){ $('select').formSelect(); }, 1000);
    
    
  }

  
  
  getUser(){
    //console.log('getUser')
    this.userService.getUser(this.id)
    .subscribe((response:User)=>{
      this.userGet = response;
      //console.log(this.userGet)
      this.getRolUser();
    },error=>{
      console.log(error)
    })
  }

  getRol(){
    //console.log('getRol')
    this.rolServide.getRols()
    .subscribe((response:Rol[])=>{
      this.arrRol = response;
      //console.log(this.arrRol);
    },error=>{
      console.log(error)
    })
  }

  getRolUser(){
    //onsole.log('getRolUser')
    //console.log(this.userGet.rol)
    this.rolServide.getRol(this.userGet.rol)
    .subscribe((response:Rol)=>{
      this.Rol = response;
      //console.log(this.Rol);
    },error=>{
      console.log(error)
    })
  }
  

  

  editUser(){
    console.log(this.userGet)
    this.userService.editUser(this.id,this.userGet)
    .subscribe((response)=>{
      console.log(response)
      this.router.navigate(['/project'])
    },error=>{
      console.log('error')
      console.log(error)
    })
  }

  isEmail()
    {
      var serchfind: boolean;
      let regexp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
      serchfind = regexp.test(this.userGet.email);
      if (!serchfind) {
        this.ValidateEmail = "invalid";

      } else {
        this.ValidateEmail = "valid";
      }
    }

    isNullNombre(input:any){
      if(input.length>0){
        this.ValidateNombre = "valid";
        this.ValidateTotal="";
      }else{
        this.ValidateNombre = "invalid";
      }
    }

    isNullApellido(input:any){
      if(input.length>0){
        this.ValidateApellido = "valid";
      }else{
        this.ValidateApellido = "invalid";
      }
    }

    isNullPass1(input:any){
      if (input.length<5) {
        this.ValidatePass="invalid";
      }else{
        this.ValidatePass="valid";
      }
    }

    cambiarRol(){
      let newRol = this.arrRol.find(Rol => Rol.Nombre==this.Rol.Nombre);
      this.userGet.rol = newRol?._id||"";
      this.ValidateTotal="";
    }
    
}
