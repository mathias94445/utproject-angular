import { Component, OnInit, Renderer2 } from '@angular/core';
import { Subject } from 'rxjs';
import { NewProject } from 'src/app/models/new-project';
import { Project } from 'src/app/models/project';
import { Rol } from 'src/app/models/rol';
import { User } from 'src/app/models/user.model';
import { ResourcesService } from 'src/app/service/resources.service';
import { RolService } from 'src/app/service/rol.service';


@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.scss']
})
export class ListUserComponent implements OnInit {
  
  model:object = {
    id:0,
    proj:""
  }
  arrRol:Rol[] = [];
  arrPro:Project[]=[];
  arrProRes:Array<any>=[];
  arrRes:User[]=[];
  arrRolRes=[];
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();
  counter=0;
  

  constructor(private rolServide:RolService,
    private ResourceService:ResourcesService,
    private renderer: Renderer2) { }

  ngOnInit(): void {
    this.getRol();
    this.getProyects();
    this.gerResources();
    setTimeout(()=>{  
      this.dtOptions = {
          pagingType: 'full_numbers',
          pageLength: 5,
          orderMulti: false,
          processing: true,

        };
    }, 10);

    
    setTimeout(function(){ $('select').formSelect(); }, 1000);
  }
  getProyectId(id:string,count:number){
    let ret:string = "";
    this.ResourceService.getProjectsResource(id)
    .subscribe((response:NewProject[])=>{
      if (typeof response[response.length - 1] === 'object') {
        ret = response[response.length - 1].NombreProyecto;
      } else {
        ret = 'Sin proyecto';
      }
      let objIndex = this.arrProRes.findIndex((obj => obj.id == count));
      this.arrProRes[objIndex].proj = ret
      
    },error=>{
      console.log(error);
    })
  }

  getProyects(){
    this.ResourceService.getProjects()
    .subscribe((response:Project[])=>{
      this.arrPro = response;
    },error=>{
      console.log(error)
    })
  }

  gerResources(){
    
    this.ResourceService.getResources()
    .subscribe((response:User[])=>{
      this.arrRes=response;
      console.log(this.arrRes)
      this.dtTrigger.next();
      this.arrRes.forEach((res)=>{
        if (res.nombre == "") {
          res.nombre = "Nombre"
          res.primerapellido = "Apellido"
        }
        let rolfd = this.arrRol.find(rolf => rolf._id == res.rol)
        res.rol = rolfd?.Nombre||"Sin Rol definido";
        this.arrProRes.push({id: this.counter, proj: ''})
        this.getProyectId(res._id,this.counter);
        this.counter++;
        
      })
      console.log(this.arrProRes)
    },error=>{
      console.log(error)
    })
    
  }

  getRol(){
    //console.log('getRol')
    this.rolServide.getRols()
    .subscribe((response:Rol[])=>{
      this.arrRol = response;
      //console.log(this.arrRol);
    },error=>{
      console.log(error)
    })
  }
  cambiarRol(id_proj:any,id_user:any){
    
    id_proj = this.arrPro.find(p=>p.NombreProyecto == id_proj)?._id||"";
    this.ResourceService.addResourceToProject(id_proj,id_user).
    subscribe(()=>{
      console.log('Asignado')
    },error=>{
      console.log(error)
    })
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
  
  ngAfterViewInit(): void {
    this.renderer.listen('document', 'click', (event) => {
      if (event.target.hasAttribute("tabindex")) {
        setTimeout(function(){ $('select').formSelect(); }, 10);
      }
    });
  }
  

}


