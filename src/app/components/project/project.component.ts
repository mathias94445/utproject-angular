import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/service/user.service';
import { SharedServiceService } from 'src/app/service/shared-service.service';
import { ProjectService } from 'src/app/service/project.service';
import { Project } from 'src/app/models/project';
import { TypeProjectService } from 'src/app/service/type-project.service';
import { TypeProject } from 'src/app/models/type-project';
import { NewProject } from 'src/app/models/new-project';



@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss']
})

export class ProjectComponent implements OnInit {

  user:User=new User();
  falgEdit:boolean=false;
  projects:Project[] = [];
  typeProject:TypeProject = new TypeProject();
  arrtypeProjects:TypeProject[] = [];
  projectSelect:Project = new Project();
  flagTypeProcess:string = "";
  flagValidate:string = "disabled";
  newP = new NewProject();

  projectSelectF = {
    name :"",
    fechaIn:"",
    fechaFn:"",
    PorctajeR:"",
    PorctajeE:"",
    estado:"",
    color:"",
    icon:""
  };
  project = {
    name :"",
    fechaIn:"",
    fechaFn:"",
    PorctajeR:"",
    PorctajeE:"",
    estado:"",
    color:"",
    icon:""
  };
  listProject = [] as any;
  folder_open:number = 999999;

  constructor(
    public userService:UserService,
    private router: Router,
    public shared:SharedServiceService,
    private projectServer:ProjectService,
    private typeProjectService:TypeProjectService
    ) { }

  ngOnInit(): void {
    this.getUser();
    this.getPrjects();
    this.gettypeProjects();
    setTimeout(function(){ $('.input-field label').addClass('active'); }, 1);
    setTimeout(function(){ $('#slec').removeClass('active'); }, 1);
    document.addEventListener('DOMContentLoaded', function() {
      var elems = document.querySelectorAll('select');
      var instances = M.FormSelect.init(elems);
    });
    setTimeout(function(){ $('select').formSelect(); }, 1000);
    //this.loopProj();
    document.addEventListener('DOMContentLoaded', function() {
      var elems = document.querySelectorAll('select');
      var elems2 = document.querySelectorAll('.datepicker');
      var instances = M.FormSelect.init(elems);
      var instances2 = M.Datepicker.init(elems);
    });
    setTimeout(function(){ $('select').formSelect(); }, 1000);
 
  }


  getUser(){
    let id:any = localStorage.getItem('id');
    this.userService.getUser(id)
    .subscribe((response:User)=>{
      this.user = response;
      this.shared.emitChange(this.user);
      //console.log(this.user);
    },error=>{
      console.log(error)
    })
  }
  getPrjects(){
    this.projectServer.getProjects()
    .subscribe((response:Project[])=>{
      this.projects = response
      this.projects.forEach( p => this.addProj(p))
    },error=>{
      console.log(error);
    })

  }
  addProj(p:Project){
    this.project = {
      name :"",
      fechaIn:"",
      fechaFn:"",
      PorctajeR:"",
      PorctajeE:"",
      estado:"",
      color:"",
      icon:""
    };
    this.project.name=p.NombreProyecto;
    this.project.PorctajeR=p.PorcentajeAvance;
    this.project.PorctajeE=p.PorcentajeAvance;
    this.project.icon="folder";
    let hoy: Date = new Date();
    let fecInicio: Date = new Date(p.FechaInicioProyecto);
    let fecFin: Date = new Date(p.FechaFinProyecto);
    let dateOptions = { year: 'numeric',month: '2-digit', day: '2-digit' } as const;
    this.project.fechaIn = fecInicio.toLocaleDateString('es-Es',dateOptions)
    this.project.fechaFn = fecFin.toLocaleDateString('es-Es',dateOptions)
    if (fecInicio.getTime()>=hoy.getTime()) {
      this.project.estado= "En Proceso";
      this.project.color= "green";
      this.project.PorctajeE = "0";
    }else if (fecFin.getTime()<=hoy.getTime()){
      this.project.estado= "Cerrado";
      this.project.color= "red";
      this.project.PorctajeE = "100";
    }else{
      let timeInMilisec: number = hoy.getTime() - fecInicio.getTime();
      let diasTrans: number = Math.ceil(timeInMilisec / (1000 * 60 * 60 * 24));
      let timeInMilisec2: number = fecFin.getTime() - fecInicio.getTime();
      let diasDura: number = Math.ceil(timeInMilisec2 / (1000 * 60 * 60 * 24));
      var porctEstimado = 0;
      if (diasDura>+p.Duracion){
        porctEstimado = (diasTrans/diasDura)*100
      }else{
        porctEstimado = (diasTrans/+p.Duracion)*100
      }
      porctEstimado = Math.round(porctEstimado)
      this.project.PorctajeE=porctEstimado+"";
      if (porctEstimado>=+p.PorcentajeAvance) {
        this.project.estado= "Atrazado";
        this.project.color= "orange";
      } else {
        this.project.estado= "En Proceso";
        this.project.color= "green";
      }
      
    }
    this.listProject.push(this.project)
    
  }

  openFolder(id:any){
    this.flagTypeProcess="edit";
    if (this.folder_open != id && this.folder_open!=999999) {
      this.listProject[this.folder_open].icon = "folder"
    } 
    this.folder_open=id
    
    if (this.listProject[id].icon == "folder_open"){
      this.listProject[id].icon = "folder"
      this.falgEdit=false;
    }else{
      this.listProject[id].icon = "folder_open"
      this.falgEdit=true;
      setTimeout(function(){ $('.datepicker').datepicker({
        format:'dd/mm/yyyy',
        i18n: {
          months:['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Setiembe','Obtubre','Noviembre','Diciembre'],
          weekdaysAbbrev:['D','L','M','M','J','V','S'],
          monthsShort:['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Set','Obt','Nov','Dic'],
          weekdays:['Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado'],
          weekdaysShort:['Dom','Lun','Mar','Mie','Jue','Vie','Sab']
        }
      }); }, 10);
      this.projectSelect =  this.projects[id];
      this.projectSelect.FechaFinProyecto = this.listProject[id].fechaIn
      this.projectSelect.FechaInicioProyecto = this.listProject[id].fechaIn
      this.gettypeProject(this.projectSelect.TipoProyecto);
      setTimeout(function(){ $('select').formSelect(); }, 1000);
      this.editOrCreateProject('edit');
    }
    
  }


  gettypeProject(id:string){
    this.typeProjectService.getTypeProject(id)
    .subscribe((response:TypeProject)=>{
      this.typeProject = response;
    },error=>{
      console.log(error)
    })
    
  }
  gettypeProjects(){
    this.typeProjectService.getTypeProjects()
    .subscribe((response:TypeProject[])=>{
      this.arrtypeProjects = response;
      console.log(this.arrtypeProjects)
    },error=>{
      console.log(error)
    })
    
  }
  cambiarTypeProject(){
    let newTypeProject = this.arrtypeProjects.find(type => type.Nombre==this.typeProject.Nombre);
    this.projectSelect.TipoProyecto = newTypeProject?._id||"";
    
  }

  changeValue(){

      this.newP.NombreProyecto = this.projectSelect.NombreProyecto
      this.newP.CantidadAsignados = this.projectSelect.CantidadAsignados
      this.newP.Costo = this.projectSelect.Costo
      this.newP.Duracion = this.projectSelect.Duracion
      this.newP.PorcentajeAvance = this.projectSelect.PorcentajeAvance
      this.newP.TipoProyecto = this.projectSelect.TipoProyecto
      console.log(this.newP)
    if (this.flagTypeProcess=="edit") {
      this.flagValidate=""
    } else {
      if (this.newP.CantidadAsignados==''||this.newP.Costo==''
        ||this.newP.Duracion==''||this.newP.FechaFinProyecto==''
        ||this.newP.FechaFinProyecto==''||this.newP.NombreProyecto==''
        ||this.newP.PorcentajeAvance==''||this.newP.TipoProyecto==''
        ){
          console.log('vacio')
          this.flagValidate="disabled"
        }else{
          this.flagValidate=""
        }
    }

  }

  editOrCreateProject(type:string){
    this.flagTypeProcess=type;
    if (type=="edit") {
      setTimeout(function(){ $('select').formSelect(); }, 10);
      setTimeout(() =>{ $('#datepicker1').datepicker({
        format:'dd/mm/yyyy',
        i18n: {
          months:['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Setiembe','Obtubre','Noviembre','Diciembre'],
          weekdaysAbbrev:['D','L','M','M','J','V','S'],
          monthsShort:['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Set','Obt','Nov','Dic'],
          weekdays:['Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado'],
          weekdaysShort:['Dom','Lun','Mar','Mie','Jue','Vie','Sab']
        },
        onSelect:(s)=>{this.newP.FechaInicioProyecto = s.toISOString()}
      }); }, 100);
      setTimeout(() =>{ $('#datepicker2').datepicker({
        format:'dd/mm/yyyy',
        i18n: {
          months:['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Setiembe','Obtubre','Noviembre','Diciembre'],
          weekdaysAbbrev:['D','L','M','M','J','V','S'],
          monthsShort:['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Set','Obt','Nov','Dic'],
          weekdays:['Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado'],
          weekdaysShort:['Dom','Lun','Mar','Mie','Jue','Vie','Sab']
        },
        onSelect:(s)=>{this.newP.FechaFinProyecto = s.toISOString()}
      }); }, 100);
    } else {
      this.projectSelect = new Project();
      this.typeProject = new TypeProject();
      this.falgEdit=true;
      setTimeout(function(){ $('select').formSelect(); }, 10);
      setTimeout(() =>{ $('#datepicker1').datepicker({
        format:'dd/mm/yyyy',
        i18n: {
          months:['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Setiembe','Obtubre','Noviembre','Diciembre'],
          weekdaysAbbrev:['D','L','M','M','J','V','S'],
          monthsShort:['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Set','Obt','Nov','Dic'],
          weekdays:['Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado'],
          weekdaysShort:['Dom','Lun','Mar','Mie','Jue','Vie','Sab']
        },
        onSelect:(s)=>{this.newP.FechaInicioProyecto = s.toISOString()}
      }); }, 100);
      setTimeout(() =>{ $('#datepicker2').datepicker({
        format:'dd/mm/yyyy',
        i18n: {
          months:['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Setiembe','Obtubre','Noviembre','Diciembre'],
          weekdaysAbbrev:['D','L','M','M','J','V','S'],
          monthsShort:['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Set','Obt','Nov','Dic'],
          weekdays:['Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado'],
          weekdaysShort:['Dom','Lun','Mar','Mie','Jue','Vie','Sab']
        },
        onSelect:(s)=>{this.newP.FechaFinProyecto = s.toISOString()}
      }); }, 100);

    }
    
  }

  submitP(){
      
      if (this.flagTypeProcess == "edit") {
        
        if(this.newP.FechaFinProyecto==''){
          this.newP.FechaFinProyecto = this.projectSelect.FechaFinProyecto
        }
        if(this.newP.FechaInicioProyecto==''){
          this.newP.FechaInicioProyecto = this.projectSelect.FechaInicioProyecto
        }
        console.log(this.newP)
        this.projectServer.putProjectId(this.projectSelect._id,this.newP)
        .subscribe((response)=>{
        },error=>{
          console.log(error)
        })
      } else {
        
        this.projectServer.addProject(this.newP)
        .subscribe((response)=>{
        },error=>{
          console.log(error)
        })
      }
      this.falgEdit=false;
      window.location.reload();
    }

  cerrar(){
    this.listProject.forEach((element: { name :string,
    fechaIn:string,
    fechaFn:string,
    PorctajeR:string,
    PorctajeE:string,
    estado:string,
    color:string,
    icon:string }) => {
      element.icon="folder";
    });
    this.falgEdit=false;
  }
}
