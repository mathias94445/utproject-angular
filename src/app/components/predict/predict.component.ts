import { Component, OnInit } from '@angular/core';
declare var require: any
import { ProjectService } from 'src/app/service/project.service';
import { Project } from 'src/app/models/project';
import { Subject } from 'rxjs';


@Component({
  selector: 'app-predict',
  templateUrl: './predict.component.html',
  styleUrls: ['./predict.component.scss']
})
export class PredictComponent implements OnInit {

  rest:any;
  projects:Project[] = [];
  tf = require('@tensorflow/tfjs');
  predict:number[] = []
  predicted1:number[] = []
  predicted2:number[] = []
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();
  constructor(private projectServer:ProjectService) { }

  ngOnInit(): void {

    this.getPrjects();
    setTimeout(()=>{  
      this.dtOptions = {
          pagingType: 'full_numbers',
          pageLength: 5,
          orderMulti: false,
          processing: true,

        };
    }, 10);
  }


  async predictCost(num:number[]){
    
    const xs = this.tf.tensor1d(num);
    let model = await this.tf.loadLayersModel("assets/json/model.json");
    let xy = model.predict(xs);
    return xy.data()
    
  }

  getPrjects(){
    let dateOptions = { year: 'numeric',month: '2-digit', day: '2-digit' } as const;
    
    this.projectServer.getProjects()
    .subscribe((response:Project[])=>{
      this.projects = response
      this.projects.forEach( p =>{
        p.FechaFinProyecto = new Date(p.FechaFinProyecto).toLocaleDateString('es-Es',dateOptions)
        p.FechaInicioProyecto = new Date(p.FechaInicioProyecto).toLocaleDateString('es-Es',dateOptions)
        this.predict.push((+p.Duracion-2)/(119-2))
      })
      console.log(this.predict)
      let z = this.predictCost(this.predict);
      z.then((r)=>{
        this.predicted1 = r;
        this.predicted1.forEach( p => {
          this.predicted2.push(Math.round(((+p*(32400-10))+10) * 100) / 100);
        })
        this.dtTrigger.next();

      })
    },error=>{
      console.log(error);
    })
    

  }
  ngAfterViewInit(): void {
    setTimeout(function(){ $('select').formSelect(); }, 10);
     
  }
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
}
