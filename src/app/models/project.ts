export class Project {

    constructor(_id = "",NombreProyecto = "",FechaInicioProyecto = "",FechaFinProyecto = "",TipoProyecto = "",CantidadAsignados ="",Duracion="",Costo="",PorcentajeAvance="",
                createdAt="",updatedAt="",__v = "") {
        this._id = _id;
        this.NombreProyecto = NombreProyecto;
        this.FechaInicioProyecto = FechaInicioProyecto;
        this.FechaFinProyecto = FechaFinProyecto;
        this.TipoProyecto = TipoProyecto;
        this.CantidadAsignados = CantidadAsignados;
        this.Duracion = Duracion;
        this.Costo = Costo;
        this.PorcentajeAvance = PorcentajeAvance;
        this.createdAt = createdAt; 
        this.updatedAt = updatedAt;
        this.__v =__v;

      }
        _id : string;
        NombreProyecto : string;
        FechaInicioProyecto : string;
        FechaFinProyecto : string;
        TipoProyecto : string;
        CantidadAsignados : string;
        Duracion : string;
        Costo : string;
        PorcentajeAvance : string;
        createdAt : string;
        updatedAt : string;
        __v :string;
}
