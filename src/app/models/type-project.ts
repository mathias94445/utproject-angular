export class TypeProject {

    constructor(_id = "",Nombre = "",Descripcion = "",__v = "") {
        this._id = _id;
        this.Nombre = Nombre;
        this.Descripcion = Descripcion;
        this.__v = __v;
        
        
      }
    
      _id:             string;
      Nombre:          string;
      Descripcion:     string;
      __v:             string;
}
