export class User {
    constructor(nombre = "Nombre",primerapellido = "Apellido",segundoapellido = "Apellido",email = "", rol = "", _id = "") {
        this.nombre = nombre;
        this.primerapellido = primerapellido;
        this.segundoapellido = segundoapellido;
        this.email = email;
        this.rol = rol;
        this._id = _id;
        
      }
    
      nombre:                string;
      primerapellido?:        string;
      segundoapellido?:        string;
      email:                  string;
      rol:                    string;
      _id:                    string;
    
}
